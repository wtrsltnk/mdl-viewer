#ifndef INPUTSTATE_H
#define INPUTSTATE_H

#include <glm/glm.hpp>

typedef struct sInputState
{
    glm::vec2 moveDirection = glm::vec2(0.0f);
    glm::vec2 lookDirection = glm::vec2(0.0f);

} tInputState;

#endif // INPUTSTATE_H
