#ifndef RENDERER_H
#define RENDERER_H

#include <glhdrs.h>
#include <vector>

class Renderer
{
  protected:
	GLuint _programObject;
	std::vector < float >_vertices;
	std::vector < float >_uvs;
};

#endif // RENDERER_H
