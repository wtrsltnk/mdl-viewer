#include "fileloader.h"

FileLoader::FileLoader(const std::string & filename):_file(std::unique_ptr < FILE, decltype(&fclose) > (fopen(filename.c_str(), "rb"), &fclose))
{
}

bool FileLoader::is_open()
{
    return _file != nullptr;
}
