#include "vertexcollection.h"

void VertexCollection::addFan(const VertexCollection & va)
{
    if (va.size() < 3)
    {
        return;
    }

    this->insert(end(), va.begin(), va.begin() + 3);

    for (size_t i = 3; i < va.size(); i++)
    {
        push_back(va[0]);
        push_back(va[i - 1]);
        push_back(va[i]);
    }
}

void VertexCollection::addStrip(const VertexCollection & va)
{
    if (va.size() < 3)
    {
        return;
    }

    insert(end(), va.begin(), va.begin() + 3);

    push_back(va[2]);
    push_back(va[1]);
    push_back(va[0]);
    for (size_t i = 3; i < va.size(); i++)
    {
        push_back(va[i - 2]);
        push_back(va[i - 1]);
        push_back(va[i]);
    }
}
