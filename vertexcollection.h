#ifndef VERTEXCOLLECTION_H
#define VERTEXCOLLECTION_H

#include <vector>
#include <glm/glm.hpp>

typedef struct
{
    glm::vec3 pos;
    glm::vec3 normal;
    glm::vec3 uvBone;
} Vertex;

class VertexCollection:public std::vector < Vertex >
{
  public:

    void addFan(const VertexCollection & va);
    void addStrip(const VertexCollection & va);
};

#endif // VERTEXCOLLECTION_H
