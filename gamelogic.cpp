#include "gamelogic.h"

#include "log.h"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

void SpawnInfoPlayerStart(tEntityClass & entityClass, std::uint32_t e, entt::DefaultRegistry & registry)
{
    TransformComponent transform;

    std::istringstream(entityClass.values["origin"]) >> transform.origin.x >> transform.origin.y >> transform.origin.z;
    std::istringstream(entityClass.values["angles"]) >> transform.angles.x >> transform.angles.y >> transform.angles.z;
    registry.assign < TransformComponent > (e, transform);

    PlayerStartComponent playerStart;
    playerStart.team = entityClass.classname == "info_player_start" ? 0 : 1;
    registry.assign < PlayerStartComponent > (e, playerStart);
}

void SpawnFuncWall(tEntityClass & entityClass, std::uint32_t e, entt::DefaultRegistry & registry)
{
    ModelComponent model;
    char c;
    std::istringstream(entityClass.values["model"]) >> c >> model.modelIndex;
    registry.assign < ModelComponent > (e, model);

    registry.assign < StaticComponent > (e, model.modelIndex);

    RenderComponent render;
    std::istringstream(entityClass.values["rendercolor"]) >> render.color.x >> render.color.y >> render.color.z;
    std::istringstream(entityClass.values["renderamt"]) >> render.amount;
    std::istringstream(entityClass.values["rendermode"]) >> render.mode;
    registry.assign < RenderComponent > (e, render);
}

void GameLogic::Init()
{
    _entityFactories.insert(std::make_pair("func_wall", SpawnFuncWall));
    _entityFactories.insert(std::make_pair("info_player_start", SpawnInfoPlayerStart));
    _entityFactories.insert(std::make_pair("info_player_deathmatch", SpawnInfoPlayerStart));
    
    _cameraEntity = _registry.create();
        auto tc = TransformComponent();
        tc.origin = glm::vec3(0.0f, 0.0f, -40.0f);
        //tc.angles.x -= 90.0f;
        _registry.assign < TransformComponent > (_cameraEntity, tc);

}

void GameLogic::SpawnEntities(std::unique_ptr < char[] > &entityDataRef)
{
    int i = 0;
    char *entityData = entityDataRef.get();

    while (entityData != 0)
    {
        auto d = GetEntity(&entityData);
        if (d.values.empty())
        {
            Log::Error("ran in to empty key/valeu map, stop spawning entities now...");
            break;
        }

        auto e = _registry.create();
        _registry.assign < ClassComponent > (e, d.classname);

        if (_entityFactories.count(d.classname))
        {
            _entityFactories[d.classname] (d, e, _registry);
        }
    }

    auto staticComponents = _registry.view < StaticComponent > ();

  for (auto e:staticComponents)
    {
        auto sc = _registry.get < StaticComponent > (e);
    }

    // add player instance containing the camera
    auto ps = RandomEntity < PlayerStartComponent > ();

    if (ps != entt::null && _registry.has < TransformComponent > (ps))
    {
        _cameraEntity = _registry.create();
        auto tc = _registry.get < TransformComponent > (ps);
        tc.origin = -tc.origin;
        tc.angles.x -= 90.0f;
        _registry.assign < TransformComponent > (_cameraEntity, tc);

        auto debugThink =[](entt::DefaultRegistry & registry, std::uint32_t e, void *userData)->float {
            Log::Verbose("thinking");
            return 1.0f;
        };
        _registry.assign < ThinkComponent > (_cameraEntity, 1.0f, debugThink);
    }

    Log::Verbose("all done loading entities");
}

glm::mat4 GameLogic::Frame(float time, const tInputState & inputState)
{
    glm::mat4 m(1.0f);

    if (_cameraEntity != entt::null)
    {
        auto & tc = _registry.get < TransformComponent > (_cameraEntity);

        //m = UpdateFpsCamera(tc, inputState);
        m = UpdateGodCamera(tc, inputState);
    }
    else
    {
        m = glm::rotate(glm::mat4(1.0f), glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
    }

  for (auto e:_registry.view < ThinkComponent > ())
    {
        auto & think = _registry.get < ThinkComponent > (e);
        think.nextThink -= time;

        if (think.nextThink <= 0.0f)
        {
            think.nextThink = think.thinkFunction(_registry, e, think.userData);
        }

        if (think.nextThink <= 0.0f)
        {
            _registry.remove < ThinkComponent > (e);
        }
    }

    return m;
}

float godAngle = glm::radians(-45.0f) ;
float godZoom = 200.0f;

glm::mat4 GameLogic::UpdateGodCamera(TransformComponent & tc, const tInputState & inputState)
{
    godAngle += (inputState.lookDirection.x * 0.02f);
    godZoom += (inputState.lookDirection.y * 1.0f);

    tc.angles.x = glm::cos(godAngle);
    tc.angles.y = glm::sin(godAngle);
    auto offset=glm::vec3(tc.angles.x * (godZoom / 1.0f), tc.angles.y * (godZoom / 1.0f), godZoom / 2.0f);
    
    auto r = glm::lookAt(offset, glm::vec3(0.0f), glm::vec3(0.0f, 0.0f, 1.0f));

    tInputState is = inputState;
    is.moveDirection.x *= -5.0f;
    is.moveDirection.y *= -10.0f;

    auto z = tc.origin.z;

   // tc.origin = TranslateCamera(tc, r, is);
    tc.origin.z = z;

    CameraPos = tc.origin - offset;
    
    return glm::translate(r, tc.origin);
}

glm::mat4 GameLogic::UpdateFpsCamera(TransformComponent & tc, const tInputState & inputState)
{
    tc.angles.x -= (inputState.lookDirection.y / 4.0f);

    if (tc.angles.x < -180.0f)
    {
        tc.angles.x = -180.0f;
    }

    if (tc.angles.x > 0.0f)
    {
        tc.angles.x = 0.0f;
    }

    tc.angles.z -= inputState.lookDirection.x;

    glm::mat4 r = glm::rotate(glm::mat4(1.0f), glm::radians(tc.angles.x), glm::vec3(1.0f, 0.0f, 0.0f));
    r = glm::rotate(r, glm::radians(tc.angles.z), glm::vec3(0.0f, 0.0f, 1.0f));

    tc.origin = TranslateCamera(tc, r, inputState);
    
    CameraPos = tc.origin;
    
    return glm::translate(r, tc.origin);
}

glm::vec3 GameLogic::TranslateCamera(TransformComponent & tc, const glm::mat4 & r, const tInputState & inputState)
{
    glm::vec3 newpos(0.0f);

    newpos.x = tc.origin.x + glm::value_ptr(r)[2] * inputState.moveDirection.y;
    newpos.y = tc.origin.y + glm::value_ptr(r)[6] * inputState.moveDirection.y;
    newpos.z = tc.origin.z + glm::value_ptr(r)[10] * inputState.moveDirection.y;

    newpos.x += glm::value_ptr(r)[0] * inputState.moveDirection.x;
    newpos.y += glm::value_ptr(r)[4] * inputState.moveDirection.x;
    newpos.z += glm::value_ptr(r)[8] * inputState.moveDirection.x;

    return newpos;
}
