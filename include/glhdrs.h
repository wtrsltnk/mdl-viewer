
#ifdef _WIN32

#include <glad/glad.h>
#include <SDL.h>

#elif __ANDROID__

#include <GLES3/gl3.h>
#include <SDL2/SDL.h>

#endif
