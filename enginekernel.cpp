#include "enginekernel.h"

#include "log.h"

#include <glad/glad.h>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/string_cast.hpp>

static MenuItem menu = {
    1, "Main", -1,              // id, text, openItem
    {                           // items 
     {2, "Play", -1, {}},       // play
     {3, "Exit", -1, {}},       // exit
     }
};

static GLuint fontYoung;
static GLuint font3D;

bool EngineKernel::Init(const std::string & hlexeRoot, const std::string & mod)
{
    Log::Info("initializing engine");
    
    if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
    {
        Log::Error("error initializing SDL");
        Log::Error(stringify(SDL_GetError()));

        return false;
    }

    uint32_t flags = SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN;

#ifdef _WIN32
    // For antialiasing
    SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
    SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);

    // the minimum number of bits in the depth buffer; defaults to 16
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
#else
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES);

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);

    flags |= SDL_WINDOW_FULLSCREEN;
#endif

    _win = SDL_CreateWindow("GAME", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, _w, _h, flags);

    if (_win == nullptr)
    {
        Log::Error("error initializing SDL window");
        Log::Error(stringify(SDL_GetError()));

        return false;
    }

    _context = SDL_GL_CreateContext(_win);

    if (_context == nullptr)
    {
        Log::Error("error initializing SDL context");
        Log::Error(stringify(SDL_GetError()));

        return false;
    }

#ifdef _WIN32
    gladLoadGL();
#endif // _WIN32

    glClearColor(0.f, 0.6f, 1.f, 0.f);
    // glClearColor(0, 0, 0, 1.0f);

#ifdef __ANDROID__
    SDL_GetWindowSize(_win, &_w, &_h);
#endif // __ANDROID__

    _perspectiveMatrix = glm::perspective(_fov, float (_w) / float (_h), 0.1f, 4000.0f);

    _sc.UpdatePads(_w, _h);

    _grid.Init();
    _tr.Init();

    fontYoung = _tr.AddFont("assets/Young.ttf", 28.0f);
    font3D = _tr.AddFont("assets/CAPTAINS.TTF", 48.0f);

    _game.Init();

    _br.Init(hlexeRoot, mod);

    return true;
}

bool EngineKernel::Frame()
{
    _frames++;

    auto currt = std::chrono::system_clock::now();
    std::chrono::duration < float >dt = currt - _t;

    if (dt.count() > 1.0f)
    {
        _fps = stringify(_frames);
        _frames = 0;
        _t = std::chrono::system_clock::now();
    }

    _frametime = (currt - _lastt);
    _lastt = currt;

    SDL_Event event;
    while (SDL_PollEvent(&event))
    {
        switch (event.type)
        {
        case SDL_WINDOWEVENT:
            {
                if (event.window.event == SDL_WINDOWEVENT_RESIZED)
                {
                    _w = event.window.data1;
                    _h = event.window.data2;
                    glViewport(0, 0, _w, _h);
                    _perspectiveMatrix = glm::perspective(_fov, float (_w) / float (_h), 0.1f, 6000.0f);
                    _sc.UpdatePads(_w, _h);
                }
                break;
            }
        case SDL_QUIT:
            {
                // handling of close button 
                _running = false;
                break;
            }
        case SDL_FINGERDOWN:
        case SDL_FINGERUP:
        case SDL_FINGERMOTION:
            {
                _sc.HandleEvents(event.tfinger);
                break;
            }
        }
    }

    auto is = _inputState;
    _sc.UpdateInpuState(_frametime.count(), _inputState);
    if (glm::length(_inputState.moveDirection) <= 0.0001f)
    {
        _inputState.moveDirection = is.moveDirection * 0.9f;
    }
    if (glm::length(_inputState.lookDirection) <= 0.0001f)
    {
        _inputState.lookDirection = is.lookDirection * 0.9f;
    }

    auto m = _game.Frame(_frametime.count(), _inputState);
    auto pos = _game.CameraPos;

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    _grid.Render(_perspectiveMatrix * m);

    static MdlRenderer::RenderOptions options;

    _br.BeginFrame();
    _br.Render(options, _perspectiveMatrix * m);
    auto r = _br.EndFrame();

    _tr.StartFrame(_w, _h);

    _tr.SelectFont(fontYoung);
    _tr.Print(-_tr.CalculateWidth(_fps) - 20, 40, _fps);

    _tr.FinishFrame();

    SDL_GL_SwapWindow(_win);

    return _running;
}

void EngineKernel::LoadMdl(const std::string & filename)
{
    if (!_br.LoadMdl(filename))
    {
        Log::Error("unable to load mdl file");

        return;
    }
}
