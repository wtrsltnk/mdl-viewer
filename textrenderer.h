#ifndef TEXTRENDERER_H
#define TEXTRENDERER_H

#include "renderer.h"

#include <string>
#include <stb_truetype.h>
#include <map>

struct MenuItem
{
    int id = -1;
    const char *text = nullptr;
      
    int openItem = -1;
      std::vector < struct MenuItem >items;
};

struct Font
{
    float _fontSize;
    stbtt_bakedchar cdata[96];  // ASCII 32..126 is 95 glyphs
    GLuint ftex;
	std::vector < float >_vertices;
	std::vector < float >_uvs;
};

class TextRenderer
{
  public:
    TextRenderer();

    void Init();
    
    GLuint AddFont(const std::string & font, float fontSize);
    
    void SelectFont(GLuint font);
    
    void StartFrame(float w, float h);

    float CalculateWidth(const std::string & text);

    void Print(float x, float y, const std::string & text);

    void PrintMenu(const MenuItem & menu);

    void FinishFrame();

  private:
    float _w;
	GLuint _programObject;
    float _h;
    
    std::map<GLuint,Font> _fonts;
    GLuint _selectedFont = 0;
};

#endif // TEXTRENDERER_H
