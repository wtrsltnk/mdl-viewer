#ifndef FILELOADER_H
#define FILELOADER_H

#include <string>
#include <memory>
#include <vector>

class FileLoader
{
  public:
    FileLoader(const std::string & filename);

    bool is_open();

    template < class T > std::vector < T > load(int offset, int count)
    {
        fseek(_file.get(), offset, SEEK_SET);
        std::vector < T > items(count);
        fread(items.data(), sizeof(T), count, _file.get());
        return items;
    }
    template < class T > T load_one(int offset = 0)
    {
        fseek(_file.get(), offset, SEEK_SET);
        T item;
        fread(&item, sizeof(T), 1, _file.get());
        return item;
    }

  private:
    std::unique_ptr < FILE, decltype(&fclose) > _file;
};

#endif // FILELOADER_H
