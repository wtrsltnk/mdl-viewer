#include "camera.h"

#include <glm/gtc/matrix_transform.hpp>

Camera::Camera()
{
}

void Camera::Resize(float w, float h)
{
    _viewWidth = w;
    _viewHeight = h;

    _perspectiveMatrix = glm::perspective(_fov, float (_viewWidth) / float (_viewHeight), 0.1f, 4000.0f);
}


const glm::mat4 & Camera::GetProjectionMatrix()
    const
    {
        return _perspectiveMatrix;
    }
