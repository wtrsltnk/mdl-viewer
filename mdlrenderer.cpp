#include "mdlrenderer.h"

#include "common.h"
#include "log.h"
#include "shader.h"
#include "types.h"
#include "FileLoader.h"
#include "vertexcollection.h"

#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/string_cast.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/euler_angles.hpp>
#include <algorithm>

const int defaultTextureSize = 128;
static unsigned char defaultCheckerData[defaultTextureSize * defaultTextureSize * 4];
static GLuint _defaultTexture = 0;

template < int C > std::array < GLint, C > GetUniformArrayLocations(GLuint programObject, const std::string & name)
{
    std::array < GLint, C > result;

    for (int i = 0; i < C; i++)
    {
        std::stringstream ss;
        ss << name << "[" << i << "]";
        result[i] = glGetUniformLocation(programObject, ss.str().c_str());
    }
    return result;
}

void MdlRenderer::Init(const std::string & hlexeRoot, const std::string & mod)
{
    _hlexeRoot = hlexeRoot;

    if (!EndsWith(_hlexeRoot, "/") && !EndsWith(_hlexeRoot, "\\"))
    {
        _hlexeRoot = _hlexeRoot + "/";
    }

    _mod = mod;

    _programObject = LoadShaderProgram("mdl");
    glUseProgram(_programObject);
    checkGlError(" glUseProgram");

    _matrixLoc = glGetUniformLocation(_programObject, "uMatrix");
    checkGlError("glGetUniformLocation");
    _texLoc = glGetUniformLocation(_programObject, "uTex");
    _bonesLoc = GetUniformArrayLocations < BONE_COUNT > (_programObject, "uBones");
    _positionIndex = glGetAttribLocation(_programObject, "aPosition");
    _normalIndex = glGetAttribLocation(_programObject, "aNormal");
    _uvIndex = glGetAttribLocation(_programObject, "aUv");

    glGenTextures(1, &_defaultTexture);
    glBindTexture(GL_TEXTURE_2D, _defaultTexture);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    int c = 0;
    for (int i = 0; i < defaultTextureSize; i++)
    {
        for (int j = 0; j < defaultTextureSize; j++)
        {
            unsigned char value = (((i & 0x8) == 0) ^ ((j & 0x8) == 0)) * 255;
            for (int k = 0; k < 3; k++)
                defaultCheckerData[c++] = value;
        }
    }

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, defaultTextureSize, defaultTextureSize, 0, GL_RGB, GL_UNSIGNED_BYTE, defaultCheckerData);
}

void UploadTextures(std::vector < mstudiotexture_t > &textures, FileLoader & file)
{
  for (auto & texture:textures)
    {
        auto ptexture = &texture;
        auto data = file.load < byte > (ptexture->index, ptexture->width * ptexture->height);
        auto pal = file.load < byte > (ptexture->index + ptexture->width * ptexture->height, 256 * 4);

        // unsigned *in, int inwidth, int inheight, unsigned *out, int
        // outwidth,
        // int outheight;
        int outwidth, outheight;
        int i, j;
        int row1[256], row2[256], col1[256], col2[256];
        byte *pix1, *pix2, *pix3, *pix4;
        byte *tex, *out;

        // convert texture to power of 2
        for (outwidth = 1; outwidth < ptexture->width; outwidth <<= 1)
            ;

        if (outwidth > 256)
            outwidth = 256;

        for (outheight = 1; outheight < ptexture->height; outheight <<= 1)
            ;

        if (outheight > 256)
            outheight = 256;

        tex = out = (byte *) malloc(outwidth * outheight * 4);

        for (i = 0; i < outwidth; i++)
        {
            col1[i] = (int)((i + 0.25f) * (ptexture->width / (float)outwidth));
            col2[i] = (int)((i + 0.75f) * (ptexture->width / (float)outwidth));
        }

        for (i = 0; i < outheight; i++)
        {
            row1[i] = (int)((i + 0.25f) * (ptexture->height / (float)outheight)) * ptexture->width;
            row2[i] = (int)((i + 0.75f) * (ptexture->height / (float)outheight)) * ptexture->width;
        }

        // scale down and convert to 32bit RGB
        for (i = 0; i < outheight; i++)
        {
            for (j = 0; j < outwidth; j++, out += 4)
            {
                pix1 = &pal[data[row1[i] + col1[j]] * 3];
                pix2 = &pal[data[row1[i] + col2[j]] * 3];
                pix3 = &pal[data[row2[i] + col1[j]] * 3];
                pix4 = &pal[data[row2[i] + col2[j]] * 3];

                out[0] = (pix1[0] + pix2[0] + pix3[0] + pix4[0]) >> 2;
                out[1] = (pix1[1] + pix2[1] + pix3[1] + pix4[1]) >> 2;
                out[2] = (pix1[2] + pix2[2] + pix3[2] + pix4[2]) >> 2;
                out[3] = 0xFF;
            }
        }

        GLuint texnum = 0;

        glGenTextures(1, &texnum);
        glBindTexture(GL_TEXTURE_2D, texnum);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA /* ?? */ , outwidth, outheight, 0, GL_RGBA, GL_UNSIGNED_BYTE, tex);
        // glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        ptexture->index = texnum;

        free(tex);
    }
}

glm::mat4 AngleQuaternion(const glm::vec3 & angles)
{
    float angle;
    float sr, sp, sy, cr, cp, cy;

    // FIXME: rescale the inputs to 1/2 angle
    angle = angles[2] * 0.5f;
    sy = sin(angle);
    cy = cos(angle);
    angle = angles[1] * 0.5f;
    sp = sin(angle);
    cp = cos(angle);
    angle = angles[0] * 0.5f;
    sr = sin(angle);
    cr = cos(angle);

    glm::quat quaternion;

    quaternion.x = sr * cp * cy - cr * sp * sy; // X
    quaternion.y = cr * sp * cy + sr * cp * sy; // Y
    quaternion.z = cr * cp * sy - sr * sp * cy; // Z
    quaternion.w = cr * cp * cy + sr * sp * sy; // W

    return glm::toMat4(quaternion);
}

bool MdlRenderer::LoadMdl(const std::string & fileName)
{
    if (fileName.empty())
    {
        Log::Current().Error("filename ia empty");
        return false;
    }

    std::stringstream ss;

    ss << _hlexeRoot << _mod << "/models/" << fileName;

    if (!EndsWith(fileName, ".mdl", true))
    {
        ss << ".mdl";
    }

    FileLoader file(ss.str());

    if (!file.is_open())
    {
        Log::Current().Error("unable to open file");
        Log::Current().Error(ss);
        return false;
    }

    _bodyparts.clear();
    _models.clear();
    _meshes.clear();

    studiohdr_t m_pstudiohdr = file.load_one < studiohdr_t > ();
    auto bps = file.load < mstudiobodyparts_t > (m_pstudiohdr.bodypartindex, m_pstudiohdr.numbodyparts);

    Log::S() << m_pstudiohdr;

    std::vector < mstudiotexture_t > textures;
    std::vector < short >skinref;

    if (m_pstudiohdr.numtextures > 0)
    {
        textures = file.load < mstudiotexture_t > (m_pstudiohdr.textureindex, m_pstudiohdr.numtextures);
        skinref = file.load < short >(m_pstudiohdr.skinindex, m_pstudiohdr.numskinref);

        UploadTextures(textures, file);
    }
    else
    {
        auto baseFilename = ss.str();

        std::string textureFilename = baseFilename.substr(0, baseFilename.size() - 4) + "T.mdl";

        FileLoader textureFile(textureFilename);
        if (!textureFile.is_open())
        {
            Log::Current().Error("unable to open texture file");
            Log::Current().Error(textureFilename);

            return false;
        }

        auto textureFileHeader = textureFile.load_one < studiohdr_t > ();

        textures = textureFile.load < mstudiotexture_t > (textureFileHeader.textureindex, textureFileHeader.numtextures);
        skinref = textureFile.load < short >(textureFileHeader.skinindex, textureFileHeader.numskinref);

        UploadTextures(textures, textureFile);
    }

    VertexCollection vertexBuffer;

  for (auto & bp:bps)
    {
        Bodypart bodypart;
        bodypart.name = bp.name;
        bodypart.firstModel = _models.size();
        bodypart.modelCount = bp.nummodels;
        _bodyparts.push_back(bodypart);

        auto models = file.load < mstudiomodel_t > (bp.modelindex, bp.nummodels);

        Log::S() << bp;
      for (auto & mdl:models)
        {
            Log::S() << mdl;
            Model model;
            model.firstMesh = _meshes.size();

            auto meshes = file.load < mstudiomesh_t > (mdl.meshindex, mdl.nummesh);
            auto verts = file.load < glm::vec3 > (mdl.vertindex, mdl.numverts);
            auto vertBones = file.load < byte > (mdl.vertinfoindex, mdl.numverts);
            auto norms = file.load < glm::vec3 > (mdl.normindex, mdl.numnorms);

          for (auto & m:meshes)
            {
                Log::S() << m;
                float s = 1.0f / (float)textures[skinref[m.skinref]].width;
                float t = 1.0f / (float)textures[skinref[m.skinref]].height;

                Mesh mesh;
                mesh.firstVertex = vertexBuffer.size();
                mesh.skin = textures[skinref[m.skinref]].index;

                auto tris = file.load < short >(m.triindex, m.numtris * 3 * 4);
                tris.push_back(0);

                short *ptricmds = tris.data();
                int i;

                while ((i = *(ptricmds++)))
                {
                    bool isFan = i < 0;
                    if (i < 0)
                    {
                        i = -i;
                    }

                    VertexCollection vs;
                    for (; i > 0; i--, ptricmds += 4)
                    {
                        Vertex v;
                        v.uvBone = glm::vec3(ptricmds[2] * s, ptricmds[3] * t, float (vertBones[ptricmds[0]]));
                        v.pos = verts[ptricmds[0]];

                        vs.push_back(v);
                    }

                    if (isFan)
                    {
                        vertexBuffer.addFan(vs);
                    }
                    else
                    {
                        vertexBuffer.addStrip(vs);
                    }
                }
                mesh.vertexCount = vertexBuffer.size() - mesh.firstVertex;
                _meshes.push_back(mesh);
            }

            model.meshCount = _meshes.size() - model.firstMesh;
            _models.push_back(model);
        }
    }

    Log::Debug("-");

    _vertexCount = vertexBuffer.size();

    glGenBuffers(1, &_vbo);
    glBindBuffer(GL_ARRAY_BUFFER, _vbo);
    glBufferData(GL_ARRAY_BUFFER, vertexBuffer.size() * sizeof(Vertex), vertexBuffer.data(), GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    auto bones = file.load < mstudiobone_t > (m_pstudiohdr.boneindex, m_pstudiohdr.numbones);

  for (auto & bone:bones)
    {
        auto t = glm::translate(glm::mat4(1.0f), bone.pos);

        auto a = AngleQuaternion(bone.angles);

        auto m = t * a;

        if (bone.parent > -1)
        {
            m = _boneMatrices[bone.parent] * m;
        }

        _boneMatrices.push_back(m);
    }

    glm::mat4 identity(1.0f);

    for (unsigned int i = 0; i < BONE_COUNT; i++)
    {
        if (i >= _boneMatrices.size())
        {
            glUniformMatrix4fv(_bonesLoc[i], 1, false, glm::value_ptr(identity));
        }
        else
        {
            glUniformMatrix4fv(_bonesLoc[i], 1, false, glm::value_ptr(_boneMatrices[i]));
        }
    }

    auto animations = file.load < mstudioseqdesc_t > (m_pstudiohdr.seqindex, m_pstudiohdr.numseq);
    auto animationGroups = file.load < mstudioseqgroup_t > (m_pstudiohdr.seqgroupindex, m_pstudiohdr.numseqgroups);

  for (auto & animation:animations)
    {
        Animation a;
        a.frameCount = animation.numframes;
        a.fps = int(animation.fps);

        a.boneKeyframes.resize(m_pstudiohdr.numbones);

        if (animation.seqgroup == 0)
        {
            auto & pseqgroup = animationGroups[animation.seqgroup];
            auto offsets = file.load < mstudioanim_t > (animation.animindex + pseqgroup.unused2, m_pstudiohdr.numbones);

            for (int s = 0; s < m_pstudiohdr.numbones; s++)
            {
                for (int i = 0; i < 6; i++)
                {

                }
            }
        }

        _animations.insert(std::make_pair(animation.label, a));
    }
    Log::S() << "\n" << _animations.size();
    Log::Debug("loaded animations");

    glUseProgram(0);

    Log::Current().Info("loaded mdl file");
    Log::Current().Info(ss);

    return true;
}

void MdlRenderer::BeginFrame()
{
    _renderStats = RenderStats();

    glEnable(GL_DEPTH_TEST);
    glDisable(GL_CULL_FACE);

    // Use the program object
    glUseProgram(_programObject);

    glBindBuffer(GL_ARRAY_BUFFER, _vbo);

    glEnableVertexAttribArray(_positionIndex);
    glVertexAttribPointer(_positionIndex, int (3), GL_FLOAT, GL_FALSE, sizeof(Vertex), (void *)0);

    glEnableVertexAttribArray(_normalIndex);
    glVertexAttribPointer(_normalIndex, int (3), GL_FLOAT, GL_FALSE, sizeof(Vertex), (void *)(3 * sizeof(float)));

    glEnableVertexAttribArray(_uvIndex);
    glVertexAttribPointer(_uvIndex, int (3), GL_FLOAT, GL_FALSE, sizeof(Vertex), (void *)(6 * sizeof(float)));

    glActiveTexture(GL_TEXTURE0);
    glEnable(GL_TEXTURE_2D);
}

void MdlRenderer::Render(const MdlRenderer::RenderOptions & options, const glm::mat4 & matrix)
{
    glUniformMatrix4fv(_matrixLoc, 1, false, glm::value_ptr(matrix));

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, _defaultTexture);
    glUniform1i(_texLoc, 0);

  for (auto & mesh:_meshes)
    {
        glBindTexture(GL_TEXTURE_2D, mesh.skin);
        glDrawArrays(GL_TRIANGLES, (GLsizei)mesh.firstVertex, (GLint)mesh.vertexCount);
    }

    return;

  for (auto & b:_bodyparts)
    {
        for (size_t m = b.firstModel; m < b.modelCount; m++)
        {
            for (size_t f = _models[m].firstMesh; f < _models[m].meshCount; f++)
            {
                //Log::S() << "\nm=" << m << ", f=" << f;
                glBindTexture(GL_TEXTURE_2D, _meshes[f].skin);
                glDrawArrays(GL_TRIANGLES, (GLsizei)_meshes[f].firstVertex, (GLint)_meshes[f].vertexCount);
            }
        }
        //Log::Debug(b.name);
    }
}

const RenderStats & MdlRenderer::EndFrame()
{
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glUseProgram(0);

    return _renderStats;
}
