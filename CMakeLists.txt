cmake_minimum_required(VERSION 3.7)

if (WIN32)
    include(cmake/CPM.cmake)

    CPMAddPackage(
        NAME SDL2
        VERSION 2.0.12
        URL https://www.libsdl.org/release/SDL2-2.0.12.zip
        OPTIONS
            "SDL_SHARED Off"
    )

    find_package(OpenGL REQUIRED)
endif ()

project(sdl2-multifile-mdlviewer)

find_package(SDL2 REQUIRED)

add_executable(sdl2-multifile-mdlviewer WIN32
    application.cpp
    boolinq.h
    camera.cpp
    camera.h
    common.cpp
    common.h
    enginekernel.cpp
    enginekernel.h
    fileloader.cpp
    fileloader.h
    gamelogic.cpp
    gamelogic.h
    gridrenderer.cpp
    gridrenderer.h
    include/filesystem.hpp
    include/glhdrs.h
    include/stb_image.h
    include/stb_rect_pack.h
    include/stb_truetype.h
    log.cpp
    log.h
    mdlrenderer.cpp
    mdlrenderer.h
    screencontroller.cpp
    shader.cpp
    stb_image.cpp
    stb_truetype.cpp
    textrenderer.cpp
    vertexcollection.cpp
    vertexcollection.h
)

target_compile_features(sdl2-multifile-mdlviewer
    PRIVATE
        cxx_std_17
)

target_include_directories(sdl2-multifile-mdlviewer
    PUBLIC
        include
        libs/glm
        libs/entt-cpp14/src
)

if (WIN32)

    target_sources(sdl2-multifile-mdlviewer
        PUBLIC
            glad_win32.c
            include/win32/glad/glad.h
            include/win32/KHR/khrplatform.h
        )

    target_include_directories(sdl2-multifile-mdlviewer
        PUBLIC
            include/win32
    )

    target_link_libraries(sdl2-multifile-mdlviewer
        ${OPENGL_LIBRARIES}
        SDL2-static
    )

else ()

    target_sources(sdl2-multifile-mdlviewer
        PUBLIC
            glad_es.c
            include/es/glad/glad.h
            include/win32/KHR/khrplatform.h
        )

    target_include_directories(sdl2-multifile-mdlviewer
        PUBLIC
            include/es
    )

    target_link_libraries(sdl2-multifile-mdlviewer
        SDL2
        GLESv3
    )

endif ()
