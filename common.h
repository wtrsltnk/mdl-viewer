#ifndef COMMON_H
#define COMMON_H

#include <map>
#include <string>
#include <vector>

void checkGlError(const char *op);
void checkPoint(const char* file, int line);

typedef struct sEntityClass
{
    std::string classname;
    std::map < std::string, std::string > values;

} tEntityClass;

std::string GetToken(char **entityDataPtr);
tEntityClass GetEntity(char **entityDataPtr);

inline bool EndsWith(std::string const &value, std::string const &ending, bool caseInsensitive = false)
{
    if (ending.size() > value.size())
        return false;

    if (caseInsensitive)
    {
        std::equal(ending.rbegin(), ending.rend(), value.rbegin(),[](char a, char b)
                   {
                   return tolower(a) == tolower(b);}
        );
    }

    return std::equal(ending.rbegin(), ending.rend(), value.rbegin());
}

std::vector < std::string > Split(const std::string & in, char delim);

#endif // COMMON_H
