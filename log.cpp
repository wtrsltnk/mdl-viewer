#include "log.h"

static LogLevels minimalLogLevel = LogLevels::Verbose;

Log *Log::_instance = nullptr;

Log::Log()
{
}

Log & Log::Current()
{
    if (Log::_instance == nullptr)
        Log::_instance = new Log();

    return *Log::_instance;
}

Log::~Log()
{
}

std::stringstream & Log::S()
{
    return Current()._tmp;
}

void Log::SetMinimalLogLevel(LogLevels level)
{
    minimalLogLevel = level;
}

void Log::LogMessage(LogLevels level, const std::string & message)
{
    if (level < minimalLogLevel)
    {
        return;
    }

    std::stringstream ss;

    ss << _tmp.str() << "\n";
    _tmp.str(std::string());
    _tmp.clear();

    if (level == LogLevels::Verbose)
        ss << "[VRB] ";
    if (level == LogLevels::Debug)
        ss << "[DBG] ";
    if (level == LogLevels::Info)
        ss << "[INF] ";
    if (level == LogLevels::Warn)
        ss << "[WRN] ";
    if (level == LogLevels::Error)
        ss << "[ERR] ";

    ss << message;

    _logLines.push_back(ss.str());

    if (!_log.is_open())
    {
        _log.open("debug.log", std::ios_base::app);
    }

    _log << ss.str() << std::endl;
}

const std::vector < std::string > &Log::LogLines()
    const
    {
        return _logLines;
    }

    void Log::Close()
{
    if (_log.is_open())
    {
        _log.close();
    }
}

void pline(int l)
{
    Log::Current().Debug((std::string("line : ") + stringify(l)).c_str());
}

void Log::Debug(const std::string & message)
{
    Current().LogMessage(LogLevels::Debug, message);
}

void Log::Error(const std::string & message)
{
    Current().LogMessage(LogLevels::Error, message);
}

void Log::Info(const std::string & message)
{
    Current().LogMessage(LogLevels::Info, message);
}

void Log::Verbose(const std::string & message)
{
    Current().LogMessage(LogLevels::Verbose, message);
}

void Log::Warn(const std::string & message)
{
    Current().LogMessage(LogLevels::Warn, message);
}

void Log::Debug(const std::stringstream & ss)
{
    Current().LogMessage(LogLevels::Debug, ss.str());
}

void Log::Error(const std::stringstream & ss)
{
    Current().LogMessage(LogLevels::Error, ss.str());
}

void Log::Info(const std::stringstream & ss)
{
    Current().LogMessage(LogLevels::Info, ss.str());
}

void Log::Verbose(const std::stringstream & ss)
{
    Current().LogMessage(LogLevels::Verbose, ss.str());
}

void Log::Warn(const std::stringstream & ss)
{
    Current().LogMessage(LogLevels::Warn, ss.str());
}

#include <glhdrs.h>

void CheckErrors(const char *source)
{
    GLenum res = glGetError();

    if (res == GL_NO_ERROR)
    {
        return;
    }

    std::stringstream ss;

    ss << "gl error found : ";

    if (res == GL_INVALID_ENUM)
    {
        ss << "GL_INVALID_ENUM " << source;
    }
    else if (res == GL_INVALID_VALUE)
    {
        ss << "GL_INVALID_VALUE " << source;
    }
    else if (res == GL_INVALID_OPERATION)
    {
        ss << "GL_INVALID_OPERATION " << source;
    }

    Log::Error(ss.str().c_str());
}
