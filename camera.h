#ifndef CAMERA_H
#define CAMERA_H

#include <glm/glm.hpp>

class Camera
{
  public:
    Camera();

    void Resize(float w, float h);

    const glm::mat4 & GetProjectionMatrix() const;

  private:
    float _viewWidth = 100;
    float _viewHeight = 100;
    float _fov = 120.0f;

      glm::mat4 _perspectiveMatrix;

};

#endif // CAMERA_H
