#ifndef GAMELOGIC_H
#define GAMELOGIC_H

#include "common.h"
#include "inputstate.h"

#include <entt/entt.hpp>
#include <string>
#include <glm/glm.hpp>
#include <cstdlib>
#include <functional>

typedef void (*SpawnEntityFunc) (tEntityClass &, std::uint32_t, entt::DefaultRegistry &);

class GameLogic
{
  public:
    void Init();

    void SpawnEntities(std::unique_ptr < char[] > &entityData);

      glm::mat4 Frame(float time, const tInputState & inputState);
      glm::vec3 CameraPos;
  private:
      entt::DefaultRegistry _registry;
      std::map < std::string, SpawnEntityFunc > _entityFactories;
      std::uint32_t _cameraEntity = entt::null;

      glm::mat4 UpdateFpsCamera(struct TransformComponent &tc, const tInputState & inputState);
      glm::mat4 UpdateGodCamera(TransformComponent & tc, const tInputState & inputState);
      glm::vec3 TranslateCamera(TransformComponent & tc, const glm::mat4 & r, const tInputState & inputState);

      template < typename T > std::uint32_t RandomEntity()
    {
        auto view = _registry.view < T > ();

        if (view.empty())
        {
            return entt::null;
        }

        return view[std::rand() % view.size()];
    }
};

struct ClassComponent
{
    std::string classname;
};

struct ThinkComponent
{
    float nextThink = 0;
      std::function < float (entt::DefaultRegistry &, std::uint32_t, void *) > thinkFunction;
    void *userData = nullptr;
};

struct TransformComponent
{
    glm::vec3 origin = glm::vec3(0.0f);
    glm::vec3 angles = glm::vec3(-90.0f, 0.0f, 0.0f);
};

struct DynamicComponent
{
    glm::vec3 velocity = glm::vec3(0.0f);
    glm::vec3 angularVelocity = glm::vec3(0.0f);
    glm::vec3 bbmin = glm::vec3(0.0f);
    glm::vec3 bbmax = glm::vec3(0.0f);
};

struct StaticComponent
{
    int model;
};

struct RenderComponent
{
    glm::vec3 color;
    int amount;
    int mode;
};

struct ModelComponent
{
    int modelIndex;
};

struct StudioComponent
{
    std::string path;
    int skin;
};

struct SpriteComponent
{
    std::string path;
    double scale;
};

struct PlayerStartComponent
{
    int team;
};

struct TargetComponent
{
    std::string name;
};

#endif // GAMELOGIC_H
