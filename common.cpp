#include "common.h"

#include "log.h"
#include <sstream>
#include <glhdrs.h>

void checkGlError(const char *op)
{
    GLint error = glGetError();

    if (error == 0)
    {
        return;
    }

    std::stringstream ss;
    for (; error; error = glGetError())
    {
        ss << "after " << op << "() glError ";

        switch (error)
        {
        case GL_NO_ERROR:
            ss << "GL_NO_ERROR";
            break;
        case GL_INVALID_ENUM:
            ss << "GL_INVALID_ENUM";
            break;
        case GL_INVALID_VALUE:
            ss << "GL_INVALID_VALUE";
            break;
        case GL_INVALID_OPERATION:
            ss << "GL_INVALID_OPERATION";
            break;
        case GL_INVALID_FRAMEBUFFER_OPERATION:
            ss << "GL_INVALID_FRAMEBUFFER_OPERATION";
            break;
        case GL_OUT_OF_MEMORY:
            ss << "GL_OUT_OF_MEMORY";
            break;
        }
    }
    Log::Error(ss);
}

void checkPoint(const char *file, int line)
{
    std::stringstream ss;
    ss << "checkin " << file << " @" << line;
    Log::Debug(ss);
    
    checkGlError(ss.str().c_str());
}

void SkipSpaces(char **entityDataPtr)
{
    if (*entityDataPtr == 0)
    {
        return;
    }

    char *data = *entityDataPtr;

    while (data[0] <= ' ')
    {
        if (data[0] == 0)
        {
            *entityDataPtr = 0;
            return;
        }
        data++;
    }

    *entityDataPtr = data;
}

void SkipComment(char **entityDataPtr)
{
    if (*entityDataPtr == 0)
    {
        return;
    }

    char *data = *entityDataPtr;

    if (data[0] == 0)
    {
        *entityDataPtr = 0;
        return;
    }

    if (data[0] != '/' || data[1] != '/')
    {
        return;
    }

    while (data[0] != '\n')
    {
        if (data[0] == 0)
        {
            *entityDataPtr = 0;
            return;
        }
        data++;
    }

    *entityDataPtr = data;
}

std::string GetToken(char **entityDataPtr)
{
    std::stringstream ss;

    SkipSpaces(entityDataPtr);

    SkipComment(entityDataPtr);

    if (*entityDataPtr == 0)
    {
        return "";
    }

    char *data = *entityDataPtr;

    if (data[0] == '\"')
    {
        data++;
        while (data[0] != '\"')
        {
            ss << data[0];
            data++;
        }
        data++;
    }
    else
    {
        while (data[0] > ' ')
        {
            ss << data[0];
            data++;
        }
    }

    *entityDataPtr = data;

    SkipSpaces(entityDataPtr);

    SkipComment(entityDataPtr);

    // Log::Verbose(ss);

    return ss.str();
}

tEntityClass GetEntity(char **entityDataPtr)
{
    tEntityClass result;

    auto token = GetToken(entityDataPtr);

    if (token != "{")
    {
        Log::Error("unexpected token, expected {");

        *entityDataPtr = 0;

        return tEntityClass();
    }

    token = GetToken(entityDataPtr);

    while ((*entityDataPtr != 0) && token != "}")
    {
        auto value = GetToken(entityDataPtr);

        if ((*entityDataPtr == 0) || value == "}")
        {
            Log::Error("unexpected end of entity");

            *entityDataPtr = 0;

            return tEntityClass();
        }

        if (token == "classname")
        {
            result.classname = value;
        }

        result.values.insert(std::make_pair(token, value));

        token = GetToken(entityDataPtr);
    }

    return result;
}

std::vector < std::string > Split(const std::string & in, char delim)
{
    std::stringstream ss(in);
    std::vector < std::string > result;

    while (ss.good())
    {
        std::string substr;
        std::getline(ss, substr, delim);
        result.push_back(substr);
    }

    return result;
}
