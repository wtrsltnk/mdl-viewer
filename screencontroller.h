#ifndef SCREENCONTROLLER_H
#define SCREENCONTROLLER_H

#include "inputstate.h"

#include <glm/glm.hpp>
#include <glhdrs.h>

struct Pad
{
    glm::vec2 max;
    glm::vec2 min;
    bool active = false;
    unsigned int fingerId = 0;
      glm::vec2 start = glm::vec2(0.0f);
      glm::vec2 end = glm::vec2(0.0f);
};

class ScreenController
{
  public:
    void HandleEvents(const SDL_TouchFingerEvent & tfinger);
    void UpdatePads(int w, int h);
    bool HitPad(int x, int y, struct Pad &pad);
    void UpdateInpuState(float frametime, tInputState & inputState);

  private:
    struct Pad lpad;
    struct Pad rpad;
    float speed = 120.0f;
    float w, h;
};

#endif // SCREENCONTROLLER_H
