#include "screencontroller.h"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

void ScreenController::HandleEvents(const SDL_TouchFingerEvent & tfinger)
{
    switch (tfinger.type)
    {
    case SDL_FINGERDOWN:
        {
            if (HitPad(tfinger.x * w, tfinger.y * h, lpad))
            {
                lpad.active = true;
                lpad.fingerId = tfinger.fingerId;
                lpad.start = glm::vec2(tfinger.x * w, tfinger.y * h);
                lpad.end = glm::vec2(tfinger.x * w, tfinger.y * h);
            }
            if (HitPad(tfinger.x * w, tfinger.y * h, rpad))
            {
                rpad.active = true;
                rpad.fingerId = tfinger.fingerId;
                rpad.start = glm::vec2(tfinger.x * w, tfinger.y * h);
                rpad.end = glm::vec2(tfinger.x * w, tfinger.y * h);
            }
            break;
        }
    case SDL_FINGERUP:
        {
            if (lpad.active && lpad.fingerId == tfinger.fingerId)
            {
                lpad.active = false;
            }
            if (rpad.active && rpad.fingerId == tfinger.fingerId)
            {
                rpad.active = false;
            }
            break;
        }
    case SDL_FINGERMOTION:
        {
            if (lpad.active && lpad.fingerId == tfinger.fingerId)
            {
                lpad.end = glm::vec2(tfinger.x * w, tfinger.y * h);
            }
            if (rpad.active && rpad.fingerId == tfinger.fingerId)
            {
                rpad.end = glm::vec2(tfinger.x * w, tfinger.y * h);
            }
            break;
        }
    }
}

void ScreenController::UpdateInpuState(float frametime, tInputState & inputState)
{
    if (rpad.active)
    {
        inputState.lookDirection = (rpad.start - rpad.end) * 0.005f;
    }
    else
    {
        inputState.lookDirection = glm::vec3(0.0f);
    }

    if (lpad.active)
    {
        inputState.moveDirection = (lpad.start - lpad.end) * 0.01f;

        inputState.moveDirection.y *= (frametime * speed);
        inputState.moveDirection.x *= (frametime * speed);
    }
    else
    {
        inputState.moveDirection = glm::vec3(0.0f);
    }
}

void ScreenController::UpdatePads(int w, int h)
{
    this->w = w;
    this->h = h;

    if (w <= h)
    {
        lpad.min = glm::vec2(0, 0.0f);
        lpad.max = glm::vec2(w, h * 0.5f);

        rpad.min = glm::vec2(0, h * 0.6f);
        rpad.max = glm::vec2(w, h);
    }
    else
    {
        lpad.min = glm::vec2(0, 0);
        lpad.max = glm::vec2(w * 0.25f, h);

        rpad.min = glm::vec2(w * 0.75f, 0);
        rpad.max = glm::vec2(w, h);
    }
}

bool ScreenController::HitPad(int x, int y, struct Pad &pad)
{
    if (x < pad.min.x)
    {
        return false;
    }
    if (y < pad.min.y)
    {
        return false;
    }
    if (x > pad.max.x)
    {
        return false;
    }
    if (y > pad.max.y)
    {
        return false;
    }

    return true;
}
