#ifndef MDLRENDERER_H
#define MDLRENDERER_H

#include <glm/glm.hpp>
#include <string>
#include <glhdrs.h>
#include <array>
#include <map>
#include <vector>

#define BONE_COUNT 64
#define MAXSTUDIOTRIANGLES	20000   // TODO: tune this
#define MAXSTUDIOVERTS		2048    // TODO: tune this
#define MAXSTUDIOSEQUENCES	2048    // total animation sequences -- KSH
                                    // incremented
#define MAXSTUDIOSKINS		100 // total textures
#define MAXSTUDIOSRCBONES	512 // bones allowed at source movement
#define MAXSTUDIOBONES		128 // total bones actually used
#define MAXSTUDIOMODELS		32  // sub-models per model
#define MAXSTUDIOBODYPARTS	32
#define MAXSTUDIOGROUPS		16
#define MAXSTUDIOANIMATIONS	2048
#define MAXSTUDIOMESHES		256
#define MAXSTUDIOEVENTS		1024
#define MAXSTUDIOPIVOTS		256
#define MAXSTUDIOCONTROLLERS 8

struct RenderStats
{
    int renderedFaces = 0;
    int skippedBackFaces = 0;
    int skippedFlaggedFaces = 0;
};

typedef struct
{
    std::string name;
    size_t firstModel;
    size_t modelCount;
} Bodypart;

typedef struct
{
    size_t firstMesh;
    size_t meshCount;
} Model;

typedef struct
{
    size_t firstVertex;
    size_t vertexCount;
    unsigned int skin;
} Mesh;

typedef struct
{
    unsigned char startFrame;
    unsigned char frameLength;
    short value;
} Keyframe;

typedef struct
{
    std::vector < Keyframe > positionX;
    std::vector < Keyframe > positionY;
    std::vector < Keyframe > positionZ;
    std::vector < Keyframe > anglesX;
    std::vector < Keyframe > anglesY;
    std::vector < Keyframe > anglesZ;
} BoneKeyframes;

typedef struct
{
    int frameCount;
    int fps;
    std::vector < BoneKeyframes > boneKeyframes;
} Animation;

class MdlRenderer
{
  public:
    struct RenderOptions
    {
        int skin = 0;
        int sequence = 0;
        float mouth = 0.0f;
        float frame = 0.0f;
        float controllers[MAXSTUDIOCONTROLLERS] = { 0.0f };
    };

  public:
    void Init(const std::string & hlexeRoot, const std::string & mod);
    bool LoadMdl(const std::string & fileName);
    void BeginFrame();
    void Render(const RenderOptions & options, const glm::mat4 & matrix);
    const RenderStats & EndFrame();

  private:
    RenderStats _renderStats;
    std::string _hlexeRoot;
    std::string _mod;

    std::vector < Bodypart > _bodyparts;
    std::vector < Model > _models;
    std::vector < Mesh > _meshes;
    std::vector < glm::mat4 > _boneMatrices;
    std::map < std::string, Animation > _animations;

    GLuint _programObject;
    GLuint _vbo;
    size_t _vertexCount;
    GLint _matrixLoc;
    GLint _texLoc;
    std::array < GLint, BONE_COUNT > _bonesLoc;
    GLint _positionIndex;
    GLint _normalIndex;
    GLint _uvIndex;
    GLint _boneIndex;

};

#endif // MDLRENDERER_H
