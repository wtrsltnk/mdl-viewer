#include "textrenderer.h"

#include "shader.h"
#include "log.h"
#include <sstream>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

TextRenderer::TextRenderer()
{
}

void TextRenderer::Init()
{
    _programObject = LoadShaderProgram("default");
}

unsigned char ttf_buffer[1 << 20];

GLuint TextRenderer::AddFont(const std::string & font, float fontSize)
{
    Font fnt;
    unsigned char temp_bitmap[512 * 512];

    fnt._fontSize = fontSize;

    FILE *f = fopen(font.c_str(), "rb");

    if (!f)
    {
        Log::Current().Error("failed to open font file");
        return 0;
    }

    size_t readByteCount = fread(ttf_buffer, 1, 1 << 20, f);

    int r = stbtt_BakeFontBitmap(ttf_buffer, 0, fnt._fontSize, temp_bitmap, 512, 512, 32, 96, fnt.cdata);

    fclose(f);

    // can free ttf_buffer at this point
    glActiveTexture(GL_TEXTURE0);
    glGenTextures(1, &(fnt.ftex));
    glBindTexture(GL_TEXTURE_2D, fnt.ftex);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_ALPHA, 512, 512, 0, GL_ALPHA, GL_UNSIGNED_BYTE, temp_bitmap);

    // can free temp_bitmap at this point
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

    _fonts.insert(std::make_pair(fnt.ftex, fnt));

    _selectedFont = fnt.ftex;

    return fnt.ftex;
}

void TextRenderer::SelectFont(GLuint font)
{
    _selectedFont = font;
}

void TextRenderer::StartFrame(float w, float h)
{
    _w = w;
    _h = h;

  for (auto & f:_fonts)
    {
        f.second._vertices.clear();
        f.second._uvs.clear();
    }
}

float TextRenderer::CalculateWidth(const std::string & text)
{
    const char *txt = text.c_str();

    float x = 0;
    float y = 0;

    while (*txt)
    {
        if (*txt < 32)
        {
            break;
        }

        stbtt_aligned_quad q;
        stbtt_GetBakedQuad(_fonts[_selectedFont].cdata, 512, 512, *txt - 32, &x, &y, &q, 1);

        ++txt;
    }

    return x;
}

void TextRenderer::Print(float x, float y, const std::string & text)
{
    if (x < 0)
    {
        x = (_w / 2.0f) + x;
    }
    if (y < 0)
    {
        y = (_h / 2.0f) + y;
    }

    const char *txt = text.c_str();

    while (*txt)
    {
        if (*txt < 32)
        {
            break;
        }

        stbtt_aligned_quad q;
        stbtt_GetBakedQuad(_fonts[_selectedFont].cdata, 512, 512, *txt - 32, &x, &y, &q, 1);

        auto & _uvs = _fonts[_selectedFont]._uvs;
        auto & _vertices = _fonts[_selectedFont]._vertices;
        _uvs.push_back(q.s0);
        _uvs.push_back(q.t0);
        _vertices.push_back(q.x0);
        _vertices.push_back(q.y0);

        _uvs.push_back(q.s1);
        _uvs.push_back(q.t0);
        _vertices.push_back(q.x1);
        _vertices.push_back(q.y0);

        _uvs.push_back(q.s1);
        _uvs.push_back(q.t1);
        _vertices.push_back(q.x1);
        _vertices.push_back(q.y1);

        _uvs.push_back(q.s0);
        _uvs.push_back(q.t0);
        _vertices.push_back(q.x0);
        _vertices.push_back(q.y0);

        _uvs.push_back(q.s1);
        _uvs.push_back(q.t1);
        _vertices.push_back(q.x1);
        _vertices.push_back(q.y1);

        _uvs.push_back(q.s0);
        _uvs.push_back(q.t1);
        _vertices.push_back(q.x0);
        _vertices.push_back(q.y1);

        ++txt;
    }
}

void TextRenderer::PrintMenu(const MenuItem & menu)
{
    if (menu.openItem < 0)
    {
        int y = (_fonts[_selectedFont]._fontSize * 4);
      for (auto i:menu.items)
        {
            Print((_w - CalculateWidth(i.text)) / 4.0f, y, i.text);
            y += (_fonts[_selectedFont]._fontSize * 2.0f);
        }
    }
    else
    {
        PrintMenu(menu.items[menu.openItem]);
    }
}

void TextRenderer::FinishFrame()
{
    glDisable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glActiveTexture(GL_TEXTURE0);
    glEnable(GL_TEXTURE_2D);

    // Use the program object
    glUseProgram(_programObject);
    GLuint matrixUniformId = glGetUniformLocation(_programObject, "u_matrix");
    glm::mat4 m = glm::ortho(0.0f, float (_w / 2.0f), 0.0f, float (_h / 2.0f), -1.0f, 1.0f);
    glm::mat4 s = glm::scale(m, glm::vec3(1.0f, -1.0f, 1.0f));
    glm::mat4 t = glm::translate(s, glm::vec3(0.0f, -float (_h / 2.0f), 0.0f));
    glUniformMatrix4fv(matrixUniformId, 1, false, glm::value_ptr(t));

  for (auto & f:_fonts)
    {
        if (f.second._vertices.empty())
        {
            continue;
        }

        glBindTexture(GL_TEXTURE_2D, f.second.ftex);

        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, int (2), GL_FLOAT, GL_FALSE, 0, (void *)f.second._vertices.data());

        glEnableVertexAttribArray(1);
        glVertexAttribPointer(1, int (2), GL_FLOAT, GL_FALSE, 0, (void *)f.second._uvs.data());

        glDrawArrays(GL_TRIANGLES, 0, int (f.second._vertices.size() / 2));
    }
}
