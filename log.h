#ifndef LOG_H
#define LOG_H

#include <fstream>
#include <sstream>
#include <vector>

void CheckErrors(const char *source);

enum class LogLevels
{
	Verbose = 0,
	Debug = 1,
	Info = 2,
	Warn = 3,
	Error = 4,
};

class Log
{
  public:
	static Log & Current();
	  virtual ~ Log();

	const std::vector<std::string> &LogLines() const;
	void Close();
	
	static void SetMinimalLogLevel( LogLevels level);

	static void Debug(const std::string &message);
	static void Error(const std::string &message);
	static void Info(const std::string &message);
	static void Verbose(const std::string &message);
	static void Warn(const std::string &message);

	static void Debug(const std::stringstream & ss);
	static void Error(const std::stringstream & ss);
	static void Info(const std::stringstream & ss);
	static void Verbose(const std::stringstream & ss);
	static void Warn(const std::stringstream & ss);
	
	static std::stringstream& S();
  private:
	static Log *_instance;
	  Log();
	  
	  std::vector<std::string> _logLines;
	  std::ofstream _log;
	  std::stringstream _tmp;
	void LogMessage(LogLevels level, const std::string & message);

};

void pline(int l);

template < typename...Args > std::string stringify(Args &&...args)
{
	std::ostringstream str;
	(str <<... << args);
	return str.str();
}

#endif // LOG_H
