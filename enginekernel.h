#ifndef ENGINEKERNEL_H
#define ENGINEKERNEL_H

#include "mdlrenderer.h"
#include "gamelogic.h"
#include "screencontroller.h"
#include "textrenderer.h"
#include "gridrenderer.h"
#include "inputstate.h"

#include <chrono>
#include <string>
#include <glhdrs.h>

class EngineKernel
{
  public:
    bool Init(const std::string & hlexeRoot, const std::string & mod);

    bool Frame();
    
    void LoadMdl(const std::string & filename);

  private:
    MdlRenderer _br;
    GameLogic _game;
    tInputState _inputState;
    bool _running = true;
    int _w = 1024, _h = 768;
    glm::mat4 _perspectiveMatrix;
    float _fov = 120.0f;
    GridRenderer _grid;
    TextRenderer _tr;
    ScreenController _sc;
    std::chrono::duration < float >_frametime;
    std::chrono::time_point < std::chrono::system_clock > _t = std::chrono::system_clock::now();
    std::chrono::time_point < std::chrono::system_clock > _lastt = std::chrono::system_clock::now();
    int _frames = 0;
    std::string _fps = "";
    SDL_Window *_win;
    SDL_GLContext _context;

};

extern EngineKernel kernel;

#endif // ENGINEKERNEL_H
