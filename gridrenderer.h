#ifndef GRIDRENDERER_H
#define GRIDRENDERER_H

#include "renderer.h"

#include <glm/glm.hpp>
#include <vector>

class GridRenderer:public Renderer
{
  public:
    void Init();
    void Render(const glm::mat4 & matrix);

  private:
      std::vector < float >_vertices;

};

#endif // GRIDRENDERER_H
