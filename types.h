#ifndef TYPES_H
#define TYPES_H

#include <glm/glm.hpp>
#include <memory>
#include <sstream>
#include <string>

typedef unsigned char byte;

typedef struct
{
    int id;
    int version;

    char name[64];
    int length;

      glm::vec3 eyeposition;    // ideal eye position
      glm::vec3 min;            // ideal movement hull size
      glm::vec3 max;

      glm::vec3 bbmin;          // clipping bounding box
      glm::vec3 bbmax;

    int flags;

    int numbones;               // bones
    int boneindex;

    int numbonecontrollers;     // bone controllers
    int bonecontrollerindex;

    int numhitboxes;            // complex bounding boxes
    int hitboxindex;

    int numseq;                 // animation sequences
    int seqindex;

    int numseqgroups;           // demand loaded sequences
    int seqgroupindex;

    int numtextures;            // raw textures
    int textureindex;
    int texturedataindex;

    int numskinref;             // replaceable textures
    int numskinfamilies;
    int skinindex;

    int numbodyparts;
    int bodypartindex;

    int numattachments;         // queryable attachable points
    int attachmentindex;

    int soundtable;
    int soundindex;
    int soundgroups;
    int soundgroupindex;

    int numtransitions;         // animation node to animation node transition 
    // 
    // 
    // 
    // 
    // 
    // graph
    int transitionindex;
} studiohdr_t;

std::stringstream & operator<<(std::stringstream & os, const studiohdr_t dt)
{
    os << "\nstudiohdr_t :\n    " << dt.name;
    os << "\n    bones " << dt.numbones << '/' << dt.boneindex;
    os << "\n    bonecontrollers " << dt.numbonecontrollers << '/' << dt.bonecontrollerindex;
    os << "\n    hitboxes " << dt.numhitboxes << '/' << dt.hitboxindex;
    os << "\n    seq " << dt.numseq << '/' << dt.seqindex;
    os << "\n    seqgroup " << dt.numseqgroups << '/' << dt.seqgroupindex;
    os << "\n    textures " << dt.numtextures << '/' << dt.textureindex;
    os << "\n    skinrefs" << dt.numskinref << '/' << dt.skinindex;
    os << "\n    bodyparts " << dt.numbodyparts << '/' << dt.bodypartindex;
    os << "\n    attachments " << dt.numattachments << '/' << dt.attachmentindex;
    return os;
}

// body part index
typedef struct
{
    char name[64];
    int nummodels;
    int base;
    int modelindex;             // index into models array
} mstudiobodyparts_t;

std::stringstream & operator<<(std::stringstream & os, const mstudiobodyparts_t dt)
{
    os << "\nmstudiobodyparts_t :\n    " << dt.name;
    os << "\n    models " << dt.nummodels << '/' << dt.modelindex;
    return os;
}

// studio models
typedef struct
{
    char name[64];

    int type;

    float boundingradius;

    int nummesh;
    int meshindex;

    int numverts;               // number of unique vertices
    int vertinfoindex;          // vertex bone info
    int vertindex;              // vertex vec3_t
    int numnorms;               // number of unique surface normals
    int norminfoindex;          // normal bone info
    int normindex;              // normal vec3_t

    int numgroups;              // deformation groups
    int groupindex;
} mstudiomodel_t;

std::stringstream & operator<<(std::stringstream & os, const mstudiomodel_t dt)
{
    os << "\nmstudiomodel_t :\n    " << dt.name << '/' << dt.type;
    os << "\n    meshes " << dt.nummesh << '/' << dt.meshindex;
    os << "\n    verts " << dt.numverts << '/' << dt.vertindex;
    os << "\n    norms " << dt.numnorms << '/' << dt.normindex;
    os << "\n    groups " << dt.numgroups << '/' << dt.groupindex;
    return os;
}

// meshes
typedef struct
{
    int numtris;
    int triindex;
    int skinref;
    int numnorms;               // per mesh normals
    int normindex;              // normal vec3_t
} mstudiomesh_t;

std::stringstream & operator<<(std::stringstream & os, const mstudiomesh_t dt)
{
    os << "\nmstudiomesh_t :\n    " << dt.skinref;
    os << "\n    tris " << dt.numtris << '/' << dt.triindex;
    os << "\n    norms " << dt.numnorms << '/' << dt.normindex;
    return os;
}

// skin info
typedef struct
{
    char name[64];
    int flags;
    int width;
    int height;
    int index;
} mstudiotexture_t;

std::stringstream & operator<<(std::stringstream & os, const mstudiotexture_t dt)
{
    os << "\nmstudiotexture_t :\n    " << dt.name;
    os << "\n    width " << dt.width << 'x' << dt.height;
    return os;
}

// bones
typedef struct
{
    char name[32];              // bone name for symbolic links
    int parent;                 // parent bone
    int flags;                  // ??
    int bonecontroller[6];      // bone controller index, -1 == none
      glm::vec3 pos, angles;    // default DoF values
    float scale[6];             // scale for delta DoF values
} mstudiobone_t;

//
// demand loaded sequence groups
//
typedef struct
{
    char label[32];             // textual name
    char name[64];              // file name
    int32_t unused1;              // was "cache"  - index pointer
    int unused2;                // was "data" -  hack for group 0
} mstudioseqgroup_t;

// sequence descriptions
typedef struct
{
    char label[32];             // sequence label

    float fps;                  // frames per second 
    int flags;                  // looping/non-looping flags

    int activity;
    int actweight;

    int numevents;
    int eventindex;

    int numframes;              // number of frames per sequence

    int numpivots;              // number of foot pivots
    int pivotindex;

    int motiontype;
    int motionbone;
      glm::vec3 linearmovement;
    int automoveposindex;
    int automoveangleindex;

      glm::vec3 bbmin;          // per sequence bounding box
      glm::vec3 bbmax;

    int numblends;
    int animindex;              // mstudioanim_t pointer relative to start of
    // sequence group data
    // [blend][bone][X, Y, Z, XR, YR, ZR]

    int blendtype[2];           // X, Y, Z, XR, YR, ZR
    float blendstart[2];        // starting value
    float blendend[2];          // ending value
    int blendparent;

    int seqgroup;               // sequence group for demand loading

    int entrynode;              // transition node at entry
    int exitnode;               // transition node at exit
    int nodeflags;              // transition rules

    int nextseq;                // auto advancing sequences
} mstudioseqdesc_t;

std::stringstream & operator<<(std::stringstream & os, const mstudioseqdesc_t dt)
{
    os << "\nmstudioseqdesc_t :\n    " << dt.label;
    os << "\n    numframes " << dt.numframes << " fps " << dt.fps;
    os << "\n    seqgroup " << dt.seqgroup;
    return os;
}

typedef struct
{
    unsigned short offset[6];
} mstudioanim_t;

// animation frames
typedef union
{
    struct
    {
        byte valid;
        byte total;
    } num;
    short value;
} mstudioanimvalue_t;

class Texture
{
  public:
    virtual ~ Texture()
    {
        if (data != nullptr)
        {
            delete[]data;
            data = nullptr;
        }
    }

    std::string name;
    int width = 0;
    int height = 0;
    int bpp = 0;
    bool repeat = true;
    unsigned char *data = nullptr;
    unsigned int glIndex = 0;
};

#endif // TYPES_H
