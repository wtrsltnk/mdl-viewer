#include "gridrenderer.h"

#include "shader.h"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

void addLine(std::vector < float >&vertices,const glm::vec2 & from,const glm::vec2 & to)
{
    vertices.push_back(from.x);
    vertices.push_back(from.y);
    vertices.push_back(0.0f);

    vertices.push_back(to.x);
    vertices.push_back(to.y);
    vertices.push_back(0.0f);
}

void GridRenderer::Init()
{
    _programObject = LoadShaderProgram("grid");

    int count = 40;

    for (int i = -count; i <= count; i+=8)
    {
        addLine(_vertices, glm::vec2(float(-count), float(i)), glm::vec2 (float(count), float(i)));
        addLine(_vertices, glm::vec2(float(i), float(-count)), glm::vec2(float(i), float(count)));
    }
}

void GridRenderer::Render(const glm::mat4 & matrix)
{
    // Use the program object
    glUseProgram(_programObject);
    GLuint matrixUniformId = glGetUniformLocation(_programObject, "u_matrix");
    glUniformMatrix4fv(matrixUniformId, 1, false, glm::value_ptr(matrix));

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, int (3), GL_FLOAT, GL_FALSE, 0, (void *)_vertices.data());

    glDrawArrays(GL_LINES, 0, int (_vertices.size() / 3));
}
