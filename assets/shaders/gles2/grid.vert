attribute vec4 vPosition;

uniform mat4 u_matrix;

varying vec2 f_uv;

void main()
{
    gl_Position = u_matrix * vPosition;
}
