attribute vec4 vPosition;
attribute vec2 vTexcoords;

uniform mat4 u_matrix;

varying vec2 f_uv;

void main()
{
    gl_Position = u_matrix * vPosition;
    f_uv = vTexcoords;
}
