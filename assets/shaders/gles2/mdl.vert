attribute vec4 aPosition;
attribute vec4 aNormal;
attribute vec4 aUv;

uniform mat4 uMatrix;

uniform mat4[64] uBones;

varying vec2 vUv;

void main()
{
    int bone = int(aUv.z);
    gl_Position = uMatrix * uBones[bone] * aPosition;
    vUv = aUv.xy;
}
