precision mediump float;

uniform sampler2D tex;
varying vec2 f_uv;

void main()
{
    gl_FragColor = vec4(1.0, 1.0, 0.0, texture2D(tex, f_uv.st).a);
}

