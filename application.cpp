#include "enginekernel.h"
#include "log.h"

#ifdef _WIN32
#include <windows.h>
int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PWSTR pCmdLine, int nCmdShow)
#else
int main()
#endif
{
    Log::SetMinimalLogLevel(LogLevels::Verbose);

    EngineKernel kernel;

#ifdef _WIN32
    if (!kernel.Init("c:/games/Counter-Strike1.3/", "cstrike"))
#else
    if (!kernel.Init("/storage/emulated/0/Download/Counter-Strike1.3/", "cstrike"))
#endif
    {
        return 0;
    }
    
    kernel.LoadMdl("player/sas/sas.mdl");
    
    //kernel.LoadMdl("hostage.mdl");
    
    while (true)
    {
        if (!kernel.Frame())
        {
            break;
        }
    }

    Log::Current().Close();

    return 0;
}
